## First step
---
1. All variables stored in /group_vars/all
2. oauth_token - Oauth token from Digital Ocean
3. region - Region for droplet, example: nyc3	
4. image - Os image, example: ubuntu-18-04-x64
5. monitoring - Enable or disable DO monitoring yes / no
6. backups - Enable or disable backups yes / no
7. size - Vm size, example: s-1vcpu-1gb
8. server_hostname - Blog domain name without www, example: test.com
9. ssh_keys - Id of your ssh key on DO, you can get it by this command:

```curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer oauth_token" "https://api.digitalocean.com/v2/account/keys"```

oauth_token need change to real oauth token.

## How to run

1. ansible-playbook do.yml - This playbook create new droplet on DO and stored his ip to new_host_is file
2. ansible-playbook site.yml -i new_host_is - This playbook setup droplet with ip from new_host_is file
3. ansible-playbook cf.yml  -i new_host_is - This playbook setup dns records on cloudflare
4. ansible-playbook le.yml  -i new_host_is - This playbook setup Letsencrypt certificate
5. ansible-playbook wp2static.yml -i new_host_is - This playbook generate static site
